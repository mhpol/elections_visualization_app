from django.db import models
from django.contrib import auth


class Election(models.Model):
	electionYear = models.CharField(max_length=255)
	electionType = models.CharField(max_length	=255)
	electionDate = models.DateField()
	created_by = models.ForeignKey('auth.User')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	is_deleted = models.BooleanField(default=False)

class County(models.Model):
	countyName = models.CharField(max_length=255)
	fips = models.CharField(max_length=255)
	created_by = models.ForeignKey('auth.User')
	created_at = models.DateTimeField(editable=False)
	updated_at = models.DateTimeField(editable=False)
	is_deleted = models.BooleanField(default=False)

class Precinct(models.Model):
	pctkey = models.CharField(max_length = 15)
	created_by = models.ForeignKey('auth.User')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	is_deleted = models.BooleanField(default=False)
	county = models.ForeignKey(County)
	precinctShape = models.TextField(null = True)
	shapeType = models.CharField(max_length =50)

class OfficeType(models.Model):
	officeType = models.CharField(max_length=255)
	created_by = models.ForeignKey('auth.User')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	is_deleted = models.BooleanField(default=False)

class Office(models.Model):
	officeName = models.CharField(max_length=255)
	districtNum = models.PositiveIntegerField(null = True)
	officeType = models.ForeignKey(OfficeType)
	created_by = models.ForeignKey('auth.User')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	is_deleted = models.BooleanField(default=False)

class Person(models.Model):
	fullname = models.CharField(max_length = 255)
	created_by = models.ForeignKey('auth.User', related_name='+')
	created_at = models.DateTimeField(editable=False)
	updated_at = models.DateTimeField(editable=False)
	is_deleted = models.BooleanField(default=False)

class OfficeElection(models.Model):
	office = models.ForeignKey(Office, related_name = 'officeelections')
	election = models.ForeignKey(Election)
	created_by = models.ForeignKey('auth.User')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	is_deleted = models.BooleanField(default=False)

class OfficeElectionHistory(models.Model):
	officeElection = models.ForeignKey(OfficeElection, related_name = 'officeElectionHistory')
	person = models.ForeignKey(Person)
	party = models.CharField(max_length=100)
	voteCount = models.PositiveIntegerField()
	created_by = models.ForeignKey('auth.User')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	is_deleted = models.BooleanField(default=False)

class District(models.Model):
	office = models.ForeignKey(Office)
	precinct = models.ForeignKey(Precinct)
	created_by = models.ForeignKey('auth.User')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	is_deleted = models.BooleanField(default=False)

class ElectionPrecinct(models.Model):
	precinct = models.ForeignKey(Precinct)
	election = models.ForeignKey(Election)
	population = models.PositiveIntegerField(null = True)
	hisPop = models.PositiveIntegerField(null = True)
	voterReg = models.PositiveIntegerField(null = True)
	turnOut = models.PositiveIntegerField(null = True)
	topVote = models.PositiveIntegerField(null = True)
	created_by = models.ForeignKey('auth.User')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	is_deleted = models.BooleanField(default=False)

class OfficeElectionByPrecinct(models.Model):
	precinct = models.ForeignKey(Precinct)
	electionPrecinct = models.ForeignKey(ElectionPrecinct)
	officeElection = models.ForeignKey(OfficeElection)
	person = models.ForeignKey(Person)
	party = models.CharField(max_length = 255)
	votes = models.PositiveIntegerField(null = True)
	created_by = models.ForeignKey('auth.User')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	is_deleted = models.BooleanField(default=False)

class PrecinctDemographics(models.Model):
	precinct = models.ForeignKey(Precinct)
	pop_anglo = models.PositiveIntegerField(null = True)	
	pop_other = models.PositiveIntegerField(null = True)
	pop_hisp = models.PositiveIntegerField(null = True)
	pop_total = models.PositiveIntegerField(null = True)
	vap_total = models.PositiveIntegerField(null = True)
	pop_black = models.PositiveIntegerField(null = True)
	pop_blhisp = models.PositiveIntegerField(null = True)
	vap_anglo = models.PositiveIntegerField(null = True)
	vap_hisp = models.PositiveIntegerField(null = True)
	vap_blhisp = models.PositiveIntegerField(null = True)
	vap_black = models.PositiveIntegerField(null = True)
	vap_other = models.PositiveIntegerField(null = True)
	created_by = models.ForeignKey('auth.User')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	is_deleted = models.BooleanField(default=False) 

class ElectionResult(models.Model):
	race_id = models.CharField(max_length = 255)
	eyear = models.CharField(max_length = 255)
	candname = models.CharField(max_length = 255)
	state = models.CharField(max_length = 255)
	district = models.CharField(max_length = 255)
	votes = models.PositiveIntegerField(null = True)
	party = models.CharField(max_length = 255)


class USState(models.Model):
	abbrev = models.CharField(max_length = 10)
	fullname = models.CharField(max_length = 255)









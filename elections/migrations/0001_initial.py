# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-15 19:52
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='County',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('countyName', models.CharField(max_length=255)),
                ('fips', models.CharField(max_length=255)),
                ('created_at', models.DateTimeField(editable=False)),
                ('updated_at', models.DateTimeField(editable=False)),
                ('is_deleted', models.BooleanField(default=False)),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='District',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Election',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('electionYear', models.CharField(max_length=255)),
                ('electionType', models.CharField(max_length=255)),
                ('electionDate', models.DateField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ElectionPrecinct',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('population', models.PositiveIntegerField(null=True)),
                ('hisPop', models.PositiveIntegerField(null=True)),
                ('voterReg', models.PositiveIntegerField(null=True)),
                ('turnOut', models.PositiveIntegerField(null=True)),
                ('topVote', models.PositiveIntegerField(null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('election', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='elections.Election')),
            ],
        ),
        migrations.CreateModel(
            name='Office',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('officeName', models.CharField(max_length=255)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='OfficeElection',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('election', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='elections.Election')),
                ('office', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='elections.Office')),
            ],
        ),
        migrations.CreateModel(
            name='OfficeElectionByPrecinct',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('party', models.CharField(max_length=255)),
                ('votes', models.PositiveIntegerField(null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('electionPrecinct', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='elections.ElectionPrecinct')),
                ('officeElection', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='elections.OfficeElection')),
            ],
        ),
        migrations.CreateModel(
            name='OfficeElectionHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('party', models.CharField(max_length=100)),
                ('voteCount', models.PositiveIntegerField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('officeElection', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='elections.OfficeElection')),
            ],
        ),
        migrations.CreateModel(
            name='OfficeType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('officeType', models.CharField(max_length=255)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fullname', models.CharField(max_length=255)),
                ('created_at', models.DateTimeField(editable=False)),
                ('updated_at', models.DateTimeField(editable=False)),
                ('is_deleted', models.BooleanField(default=False)),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Precinct',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pctkey', models.CharField(max_length=15)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('county', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='elections.County')),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='PrecinctsDemographics',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pop_anglo', models.PositiveIntegerField(null=True)),
                ('pop_other', models.PositiveIntegerField(null=True)),
                ('pop_hisp', models.PositiveIntegerField(null=True)),
                ('pop_total', models.PositiveIntegerField(null=True)),
                ('vap_total', models.PositiveIntegerField(null=True)),
                ('pop_black', models.PositiveIntegerField(null=True)),
                ('pop_blhisp', models.PositiveIntegerField(null=True)),
                ('vap_anglo', models.PositiveIntegerField(null=True)),
                ('vap_hisp', models.PositiveIntegerField(null=True)),
                ('vap_blhisp', models.PositiveIntegerField(null=True)),
                ('vap_black', models.PositiveIntegerField(null=True)),
                ('vap_other', models.PositiveIntegerField(null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('precinct', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='elections.Precinct')),
            ],
        ),
        migrations.AddField(
            model_name='officeelectionhistory',
            name='person',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='elections.Person'),
        ),
        migrations.AddField(
            model_name='officeelectionbyprecinct',
            name='person',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='elections.Person'),
        ),
        migrations.AddField(
            model_name='officeelectionbyprecinct',
            name='precinct',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='elections.Precinct'),
        ),
        migrations.AddField(
            model_name='office',
            name='officeType',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='elections.OfficeType'),
        ),
        migrations.AddField(
            model_name='electionprecinct',
            name='precinct',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='elections.Precinct'),
        ),
        migrations.AddField(
            model_name='district',
            name='office',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='elections.Office'),
        ),
        migrations.AddField(
            model_name='district',
            name='precinct',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='elections.Precinct'),
        ),
    ]

from django.shortcuts import render, render_to_response, redirect
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.template import RequestContext
from django.db import models
from django.forms.models import model_to_dict
from django.core import serializers
from .models import *
from django.db.models import Sum, Avg, Count, F, Max, Min, Q, Prefetch
from itertools import groupby
from pprint import pprint
import cProfile, profile, ast, datetime, json


#convenience functions, not views.



#takes a model object and returns a dic. Using it to develop nested json for d3. 
def obj_to_dict(model_instance):
    serial_obj = serializers.serialize('json', [model_instance])
    obj_as_dict = json.loads(serial_obj)[0]['fields']
    # obj_as_dict['pk'] = model_instance.pk
    return obj_as_dict



def frontpage(request):
	return render(request, 'frontpage.html')
def cycles(request):
	# election_objects = elections.objects.all().order_by('-electionDate')
	return render(request,"elections/elections.html", {'page_title': 'Elections', 'elections': election_objects})

def exploreStep1(request):
	return render(request, 'elections/exploreStep1.html')

def exploreCounty(request):
	cntys = County.objects.order_by('countyName').values('fips', 'countyName').distinct()
	return render(request, 'elections/exploreCounty.html', {'page_title': 'Explore: Counties','counties': cntys})

def exploreCountyFinal(request, fips):
	c = County.objects.get(fips = fips)
	cntyPcts = County.objects.filter(fips = fips).values_list('precinct', flat = True)
	officesInCounty = District.objects.filter(precinct__in = cntyPcts).values('office__officeName', 'office__id', 'office__officeType_id').distinct()
	#shows most recent stats for precincts. Will eventually need to make this dynamic, taking a get in from election table for the most recent year.
	electionPrecincts = ElectionPrecinct.objects.filter(precinct_id__in = cntyPcts).filter(election_id = 8)
	return render(request, 'elections/exploreCountyFinal.html', {'page_title': 'Explore: ' + c.countyName + ' County','countyName': c.countyName, 'county_id': c.id, 'offices': officesInCounty, 'electionPrecincts': electionPrecincts})


def exploreDistrictInCounty(request, county_id, office_id):
	electionResults = (OfficeElectionByPrecinct.objects
		.filter(Q(officeElection__office_id = office_id) & Q(precinct__county_id = county_id))
		.values('officeElection__election__electionDate','party','person__fullname')
		.annotate(Sum('votes'))
		.order_by('-officeElection__election__electionDate'))
	county = County.objects.get(id = county_id)
	office = Office.objects.get(id = office_id)
	electionDict = dict()
	for obj in electionResults:
		electionDict.setdefault(obj['officeElection__election__electionDate'],[]).append(obj)
	print(electionDict)
	for key in electionDict:
		for x, item in enumerate(electionDict[key]):
			listitem = list(item.values())
			listitem = listitem[1:] #remove date at zero index, since that's the key
			electionDict[key][x] = listitem
		electionDict[key].sort(key = lambda z: z[-1], reverse = True) #sort the results of the election so that the winner is on top. #Note the -1 doesn't stand for reversed, but for the last element in the array.

	return render(request, 'elections/exploreDistrictInCounty.html', {'electionResults': electionDict.items(), 'county': county, 'office': office})

def exploreDistrict(request):
	ofTypes = OfficeType.objects.all()
	return render(request, 'elections/exploreDistrict.html', {"officeTypes":ofTypes})

def exploreDistrictFinal(request, officeType_id, office_id):
	officeInfo = Office.objects.get(id = office_id)
	oe = OfficeElection.objects.filter(office_id = office_id)
	oeIDs = []
	for e in oe:
		oeIDs.append(e.id)
	electionResults = OfficeElectionHistory.objects.filter(officeElection__in = oeIDs).values('officeElection__election__electionDate', 'party', 'person__fullname', 'voteCount').order_by('-officeElection__election__electionDate')
	print('post query:')
	print(electionResults)


	# precinctResults = OfficeElectionByPrecinct.objects.filter(officeElection__in = oeIDs).values()	
	#create a dictionary of lists by electionDate
	electionDict = dict()
	for obj in electionResults:
		electionDict.setdefault(obj['officeElection__election__electionDate'],[]).append(obj)
	print(electionDict)
	for key in electionDict:
		for x, item in enumerate(electionDict[key]):
			listitem = list(item.values())
			listitem = listitem[1:] #remove date at zero index, since that's the key
			electionDict[key][x] = listitem
		electionDict[key].sort(key = lambda z: z[-1], reverse = True) #sort the results of the election so that the winner is on top. #Note the -1 doesn't stand for reversed, but for the last element in the array.

	print('post_transform:')
	print(electionDict)


	return render(request, 'elections/exploreDistrictFinal.html', {'office_id': office_id, 'officeType_id': officeType_id, 'officeInfo':officeInfo, 'electionResults':electionDict.items()})

def exploreDistrictStep2(request, officeType_id):
	of = Office.objects.filter(officeType_id = officeType_id)
	return render(request, 'elections/exploreDistrictStep2.html', {'offices' : of})

def exploreElection(request):
	el = Election.objects.distinct()
	return render(request, 'elections/exploreElection.html', {"elections": el})

def exploreElectionStep2(request, election_id):
	availOfficeTypes = OfficeElection.objects.filter(election_id = election_id).values('office__officeType','office__officeType__officeType').distinct()
	return render(request, 'elections/exploreElectionStep2.html', {'officeTypes' : availOfficeTypes})

def exploreElectionStep3(request, election_id, officeType_id):
	oe = OfficeElection.objects.filter(election_id = election_id).values_list('office_id', flat = True)
	of = Office.objects.filter(id__in = oe).filter(officeType = officeType_id).values('id', 'officeName')
	return render(request, 'elections/exploreElectionStep3.html', {'offices' : of})

def exploreElectionFinal(request, election_id, officeType_id, office_id):
	electionInfo = Election.objects.get(id = election_id)
	officeInfo = Office.objects.get(id = office_id)
	print(officeInfo)
	#We know that there is only one possible row returning from this filter, but it still returns a dictionary.
	#So we need to loop through the dictionary to get to the first object.
	def officeElectionID():
		oe = OfficeElection.objects.filter(office_id = office_id, election_id = election_id)
		for idx, obj in enumerate(oe):
			if idx == 0:
				return obj.id
			else:
				break
	oeID = officeElectionID()
	electionResults = OfficeElectionHistory.objects.filter(officeElection_id = oeID)
	precincts = District.objects.filter(office_id = office_id).values('precinct__pctkey')
	print(len(precincts))
	return render(request, 'elections/exploreElectionFinal.html', {'electionInfo': electionInfo, 'officeInfo':officeInfo, 'electionResults':electionResults, 'precincts': precincts})

def districtResultChart(request, office_id):
	pastElections = OfficeElection.objects.filter(office_id = office_id)
	electionHistory = OfficeElectionHistory.objects.filter(officeElection__in = pastElections).values('officeElection__election__electionYear', 'voteCount', 'party').order_by('officeElection__election__electionDate')
	history = (electionHistory.filter(party__in = ['DEM','REP'])
		.values('officeElection__election__electionYear', 'voteCount', 'party')
		.order_by('officeElection__election__electionDate'))

	print(history)

	jsonreturn = []

	# this finds the index of dictionary with the correct year values
	def find(lst, key, value):
	    for i, dic in enumerate(lst):
	        if dic[key] == value:
	            return i
	    return -1


	yearlist = []
	for a in history:
		yearlist.append(a['officeElection__election__electionYear'])

	yearlist = list(set(yearlist))
	yearlist.sort(key = lambda x: x)

	for a in yearlist:
		jsonreturn.append({'electionYear' : int(a)})

	#populate the charts
	for a in history:
		jsonreturn[find(jsonreturn, 'electionYear', int(a['officeElection__election__electionYear']))][a['party']] = a['voteCount']

	for a in jsonreturn:
		if 'DEM' in a:
			pass
		else: 
			a['DEM'] = 0
		if  'REP' in a:
			pass
		else:
			a['REP'] = 0

	print(jsonreturn)
	return JsonResponse(jsonreturn, safe = False)


def districtDemogChart(request, office_id):
	jsonreturn = []

	epQuery = (ElectionPrecinct.objects.filter(precinct__district__office = office_id)
		.values('election', 'election__electionYear')
		.annotate(vr = Sum('voterReg'), to = Sum('turnOut'))
		.order_by('election__electionYear'))
	for idx, a in enumerate(epQuery):
		jsonreturn.append({'turnout' : a['to']})
		jsonreturn[idx]['voterRegistration'] = a['vr']
		jsonreturn[idx]['electionYear'] = int(a['election__electionYear'])

	return JsonResponse(jsonreturn, safe = False)
	

def countyDemogChart(request, county_id):
	
	jsonreturn = []
	epQuery = ElectionPrecinct.objects.filter(precinct__county = county_id).values('election', 'election__electionYear').annotate(vr = Sum('voterReg'), to = Sum('turnOut')).order_by('election__electionYear')
	for idx, a in enumerate(epQuery):
		jsonreturn.append({'turnout' : a['to']})
		jsonreturn[idx]['voterRegistration'] = a['vr']
		jsonreturn[idx]['electionYear'] = int(a['election__electionYear'])


	return JsonResponse(jsonreturn, safe = False)

def countyResultChart(request, county_id):
	
	oeQuery = (OfficeElectionByPrecinct.objects.filter(precinct__county = county_id)
		# .filter(Q(officeElection__office__id = 18)| Q(officeElection__office__id = 3)) ##GET PRESIDENT AND GOVERNOR PRECINCT ONLY 
		.values('officeElection__election', 'officeElection__election__electionYear', 'party'))
		# .annotate(Sum('votes'))
		# .order_by('officeElection__election__electionYear'))

	print(oeQuery.query)
	print(oeQuery)
	jsonreturn = []

	# # this finds the index of dictionary with the correct year values
	# def find(lst, key, value):
	#     for i, dic in enumerate(lst):
	#         if dic[key] == value:
	#             return i
	#     return -1

	# yearlist = []
	# for a in oeQuery:
	# 	yearlist.append(a['officeElection__election__electionYear'])

	# yearlist = list(set(yearlist))
	# yearlist.sort(key = lambda x: x)

	# for a in yearlist:
	# 	jsonreturn.append({'electionYear' : int(a)})

	# #populate the charts
	# for a in oeQuery:
	# 	jsonreturn[find(jsonreturn, 'electionYear', int(a['officeElection__election__electionYear']))][a['party']] = a['votes__sum']

	# for a in jsonreturn:
	# 	if 'DEM' not in a:
	# 		a['DEM'] = 0
	# 	if 'REP' not in a:
	# 		a['REP'] = 0

	# print('\n\n\n\n\n\n\n')
	# print('country results chart data')
	# print(jsonreturn)
	return JsonResponse(jsonreturn, safe = False)


def districtInCountyDemogChart(request, county, office):
	if request.POST:
		county_id = request.POST.get('county_id')
		office_id = request.POST.get('office_id')
		tobyelection = []
		vrbyelection = []
		electionYears = []
		epQuery = (ElectionPrecinct.objects
			.filter(Q(precinct__county = county_id) & Q(precinct__district__office = office_id))
			.values('election', 'election__electionYear')
			.annotate(vr = Sum('voterReg'), to = Sum('turnOut')))
		for a in epQuery:
			tobyelection.append(a['to'])
			vrbyelection.append(a['vr'])
			electionYears.append(int(a['election__electionYear']))

		jsonreturn = {
			'Turnout' : tobyelection,
			'Voter Registration' : vrbyelection,
			'electionYears' : electionYears
		}
		return JsonResponse(jsonreturn)


def districtInCountyResultChart(request, county, office):
	if request.POST:
		print('yorororororor this is the district incounty result chart ====================')
		county_id = request.POST.get('county_id')
		office_id = request.POST.get('office_id')
		oeQuery = (OfficeElectionByPrecinct.objects
			.filter(Q(precinct__county = county_id) & Q(precinct__district__office = office_id))
			.values('officeElection__election', 'officeElection__election__electionYear', 'party')
			.annotate(Sum('votes'),)
			.order_by('officeElection__election__electionYear'))

		#for building the axis
		demyearvals = []
		repyearvals = []
		#creating lists to append the data
		demlist = []
		replist = []
		
		for a in oeQuery:
			if a['party'] == 'DEM':
				demyearvals.append(a['officeElection__election__electionYear'])
				demlist.append(a['votes__sum'])
			elif a['party'] == 'REP':
				repyearvals.append(a['officeElection__election__electionYear'])
				replist.append(a['votes__sum'])
		
		jsonreturn = {
		 'Democrats':demlist,
		 'Republicans':replist,
		 'demyears': demyearvals,
		 'repyears': repyearvals,
		}

		print(jsonreturn)

		return JsonResponse(jsonreturn)


def countydynamicmapajax(request, county_id):
	county= Precinct.objects.filter(county = county_id).values_list('id', flat=True)
	precincts = PrecinctDemographics.objects.filter(precinct__in = county).values(
		"precinct__precinctShape", 
		"precinct__shapeType",
		"precinct__pctkey", 
		"precinct_id",
		"pop_hisp",
		"pop_anglo",
		"pop_other",
		"pop_total",
		"vap_total",
		"pop_black",
		"pop_blhisp",
		"vap_anglo",
		"vap_hisp",
		"vap_blhisp",
		"vap_black",
		"vap_other")
	precinctsElectionData = (ElectionPrecinct.objects.filter(precinct__in = county).values(
			"precinct__pctkey", 
			"election__electionYear",
			"election__electionType",
			"election__electionDate")
		.annotate(
			Sum("voterReg"),
			Sum("turnOut"),
			Sum("topVote")))

	ped = {}
	for i in precinctsElectionData:
		pctkey = ped.setdefault( i['precinct__pctkey'], {} )
		for k in i:
			 year = pctkey.setdefault(i["election__electionYear"], {})
			 year[k] = i[k]

	#build new dictionaries and append to list


	f = open('pec.txt', 'w+')
	f.write(str(ped))
	f.close()


	returndict = {"type": "FeatureCollection", "features":[]}
	featureslist = returndict["features"]

	print(precincts.query)

	def getPercent(a,b):
		try:
			r = round((a/b)*100, 2)
		except ZeroDivisionError:
			r = 0
		return r

	for idx, p in enumerate(precincts):
		featureslist.append({"type":"Feature",
			"geometry":{
				"type": p["precinct__shapeType"],
				"coordinates": (ast.literal_eval(p["precinct__precinctShape"]))},
			"properties": {
				"PCTKEY": p["precinct__pctkey"],
				"pop_hisp": (getPercent(int(p['pop_hisp']),int(p['pop_total']))),
				"pop_anglo": (getPercent(int(p['pop_anglo']),int(p['pop_total']))),
				"pop_other": (getPercent(int(p['pop_other']),int(p['pop_total']))),
				"pop_total": int(p['pop_total']),
				"vap_total": int(p['vap_total']),
				"pop_black": (getPercent(int(p['pop_black']),int(p['pop_total']))),
				"pop_blhisp": (getPercent(int(p['pop_blhisp']),int(p['pop_total']))),
				"vap_anglo": (getPercent(int(p['vap_anglo']),int(p['vap_total']))),
				"vap_hisp": (getPercent(int(p['vap_hisp']),int(p['vap_total']))),
				"vap_blhisp": (getPercent(int(p['vap_blhisp']),int(p['vap_total']))),
				"vap_black": (getPercent(int(p['vap_black']),int(p['vap_total']))),
				"vap_other": (getPercent(int(p['vap_other']),int(p['vap_total']))),
				}})
		try:
			properties = featureslist[idx]['properties']
			pctkey = properties['PCTKEY']
			peData = ped[pctkey]
		except KeyError:
			print('THERE\'S A MISSING KEY AT ' + str(pctkey))
			peData = {}
			continue
		properties.update(electionData = peData)


		# for i in enumerate(featureslist):
		# 	print(i['properties']['PCTKEY'])
	return JsonResponse(returndict, safe=False)



def dynamicmap(request,district):
	return render(request, 'elections/map1.html', {'district':district})


def dynamicmapajax(request, district_id):
	officedistrict = District.objects.filter(office_id = district_id).values_list('precinct', flat=True)
	precincts = PrecinctDemographics.objects.filter(precinct__in = officedistrict).values(
		"precinct__precinctShape", 
		"precinct__shapeType",
		"precinct__pctkey", 
		"precinct_id",
		"pop_hisp",
		"pop_anglo",
		"pop_other",
		"pop_total",
		"vap_total",
		"pop_black",
		"pop_blhisp",
		"vap_anglo",
		"vap_hisp",
		"vap_blhisp",
		"vap_black",
		"vap_other")
	precinctsElectionData = (ElectionPrecinct.objects.filter(precinct__in = officedistrict).values(
			"precinct__pctkey", 
			"election__electionYear",
			"election__electionType",
			"election__electionDate")
		.annotate(
			Sum("voterReg"),
			Sum("turnOut"),
			Sum("topVote")))

	ped = {}
	for i in precinctsElectionData:
		pctkey = ped.setdefault( i['precinct__pctkey'], {} )
		for k in i:
			 year = pctkey.setdefault(i["election__electionYear"], {})
			 year[k] = i[k]

	#build new dictionaries and append to list


	returndict = {"type": "FeatureCollection", "features":[]}
	featureslist = returndict["features"]

	print(precincts.query)

	def getPercent(a,b):
		try:
			r = round((a/b)*100, 2)
		except ZeroDivisionError:
			r = 0
		return r

	for idx, p in enumerate(precincts):
		featureslist.append({"type":"Feature",
			"geometry":{
				"type": p["precinct__shapeType"],
				"coordinates": (ast.literal_eval(p["precinct__precinctShape"]))},
			"properties": {
				"PCTKEY": p["precinct__pctkey"],
				"pop_hisp": (getPercent(int(p['pop_hisp']),int(p['pop_total']))),
				"pop_anglo": (getPercent(int(p['pop_anglo']),int(p['pop_total']))),
				"pop_other": (getPercent(int(p['pop_other']),int(p['pop_total']))),
				"pop_total": int(p['pop_total']),
				"vap_total": int(p['vap_total']),
				"pop_black": (getPercent(int(p['pop_black']),int(p['pop_total']))),
				"pop_blhisp": (getPercent(int(p['pop_blhisp']),int(p['pop_total']))),
				"vap_anglo": (getPercent(int(p['vap_anglo']),int(p['vap_total']))),
				"vap_hisp": (getPercent(int(p['vap_hisp']),int(p['vap_total']))),
				"vap_blhisp": (getPercent(int(p['vap_blhisp']),int(p['vap_total']))),
				"vap_black": (getPercent(int(p['vap_black']),int(p['vap_total']))),
				"vap_other": (getPercent(int(p['vap_other']),int(p['vap_total']))),
				}})
		try:
			properties = featureslist[idx]['properties']
			pctkey = properties['PCTKEY']
			peData = ped[pctkey]
		except KeyError:
			print('THERE\'S A MISSING KEY AT ' + str(pctkey))
			peData = {}
			continue
		properties.update(electionData = peData)


		# for i in enumerate(featureslist):
		# 	print(i['properties']['PCTKEY'])
	return JsonResponse(returndict, safe=False)


def officeTypeReport(request, officetype_id):
	ot = OfficeType.objects.get(id = officetype_id)
	return render(request, 'elections/officetypereport.html', {'officeType' : ot })

def officeTypeReportAjax(request):

	officeTypeName = request.GET.get('officeType')
	officeType_id = request.GET.get('officeType_id')

	results = Office.objects.raw(
		"""
	select a.id, a.officetype_id, a.officeName, a.districtNum, b.electionYear, b.electionDate, b.electionType, c.party, c.voteCount, d.fullname
	from 
		elections_office a
	right join  
		(select j.id oe_id, j.office_id, j.election_id, k.electionType, k.electionDate, k.electionYear  
				from 
					elections_officeelection j
				inner join
					(select * from elections_election) k
				on j.election_id = k.id) b 
		on 
			a.id  = b.office_id
	right join
		(select id, party, voteCount, person_id, officeelection_id from elections_officeelectionhistory) c on b.oe_id = c.officeelection_id
	left join
		(select id, fullname from elections_person) d on c.person_id = d.id
	where
		officeType_id = %s
		and
		(c.party = 'DEM' or c.party = 'REP')
	order by
		a.districtNum, b.electionDate;
		""", [officeType_id])
	

	resultslist = []
	uniqueyears = set()

	for a in results:
		d = dict()
		d['office_id'] = a.id
		d['officeName'] = a.officeName
		d['districtNum'] = a.districtNum
		d['electionYear'] = a.electionYear
		d['electionDate'] = a.electionDate
		d['electionType'] = a.electionType
		d['party'] = a.party
		d['voteCount'] = a.voteCount
		d['fullname'] = a.fullname
		resultslist.append(d)
		uniqueyears.add(a.electionYear)

	uniqueyears = list(uniqueyears)	

	def group(items, key, subs_name):
		return [{key: g, subs_name: [dict((k, v) for k, v in iter(s.items()) if k != key) 
		for s in sub]} for g, sub in groupby(sorted(items, key=lambda item: item[key]),lambda item: item[key])]

	jsonreturn = [{'districtNum': g['districtNum'],
		 'electionResults': group(g['electionResults'], "electionYear", "result")} for g in group(resultslist,"districtNum", "electionResults")]
	

	print('###right after creation... where are we')
	pprint(jsonreturn)

	for a in jsonreturn:
		a['office_id'] = (a['electionResults'][0]['result'][0]['office_id'])
		for  b in a['electionResults']:
			#if there are empty values (e.g. only one party), logic that creates zero values for the other party
			if len(b['result']) < 2:
				existparty = b['result'][0]['party']
				possibleparties = ['DEM', 'REP']
				possibleparties.remove(existparty)
				remainingparty = possibleparties.pop()
				b['result'].append({k:0 for (k,v) in b['result'][0].items()})
				#put in necessary values
				b['result'][-1]['party'] = remainingparty
			l = [{i['party']: i } for i in b['result']]
			resultsdict = l[0] if len(l) <= 1 else {**l[0], **l[1]} 
			year = b['electionYear']
			b[year] = resultsdict
			del b['result']
			del b['electionYear']
		a['electionResults'] = {k:v for d in a['electionResults'] for k, v in d.items()}


	return JsonResponse(jsonreturn, safe = False) 

		
def electionResultPage(request, state):
	print(state)
	print('###')
	s = USState.objects.get(abbrev = state.upper())
	return render(request, 'elections/electionresults.html', {'state': s})

def officeTypeReportAjax2(request):

	state = request.GET.get('state')
	results = ElectionResult.objects.filter(state=state).filter(party__in=['DEM', 'REP'])
	# results = list(results.values())
	print(results)
	resultslist = []
	uniqueyears = set()

	for a in results:
		d = dict()
		d['districtNum'] = a.district
		d['electionYear'] = a.eyear
		d['party'] = a.party
		d['voteCount'] = a.votes
		d['fullname'] = a.candname
		resultslist.append(d)
		uniqueyears.add(a.eyear)

	uniqueyears = list(uniqueyears)	

	def group(items, key, subs_name):
		return [{key: g, subs_name: [dict((k, v) for k, v in iter(s.items()) if k != key) 
		for s in sub]} for g, sub in groupby(sorted(items, key=lambda item: item[key]),lambda item: item[key])]

	jsonreturn = [{'districtNum': int(g['districtNum']),
		 'electionResults': group(g['electionResults'], "electionYear", "result")} for g in group(resultslist,"districtNum", "electionResults")]
	


	for a in jsonreturn:
		# a['office_id'] = (a['electionResults'][0]['result'][0]['office_id'])
		for  b in a['electionResults']:
			#if there are empty values (e.g. only one party), logic that creates zero values for the other party
			if len(b['result']) < 2:
				existparty = b['result'][0]['party']
				possibleparties = ['DEM', 'REP']
				possibleparties.remove(existparty)
				remainingparty = possibleparties.pop()
				b['result'].append({k:0 for (k,v) in b['result'][0].items()})
				#put in necessary values
				b['result'][-1]['party'] = remainingparty
			l = [{i['party']: i } for i in b['result']]
			resultsdict = l[0] if len(l) <= 1 else {**l[0], **l[1]} 
			year = b['electionYear']
			b[year] = resultsdict
			del b['result']
			del b['electionYear']
		a['electionResults'] = {k:v for d in a['electionResults'] for k, v in d.items()}


	return JsonResponse(jsonreturn, safe = False) 

		


	





# main/urls.py
from django.conf.urls import url,include
from . import views

# We are adding a URL called /home
urlpatterns = [

	url(r'^cycles/$', views.cycles, name='elections-cycles'),
	url(r'explore/$', views.exploreStep1),
    url(r'explore/county/$', views.exploreCounty),
    url(r'explore/county/([0-9]{1,3})/$', views.exploreCountyFinal),
   	url(r'explore/county/officeview/([0-9]{1,3})/([0-9]{1,3})/$', views.exploreDistrictInCounty),
 	url(r'explore/district/$', views.exploreDistrict),
 	url(r'explore/district/([0-9]{1,2})/$', views.exploreDistrictStep2),
 	url(r'explore/district/([0-9]{1,2})/([0-9]{1,3})/$', views.exploreDistrictFinal),
 	url(r'explore/election/$', views.exploreElection),
 	url(r'explore/election/([0-9]{1,2})/$', views.exploreElectionStep2),
 	url(r'explore/election/([0-9]{1,2})/([0-9]{1,3})/$', views.exploreElectionStep3),
 	url(r'explore/election/([0-9]{1,2})/([0-9]{1,3})/([0-9]{1,3})/$', views.exploreElectionFinal),
 	url(r'ajax/district_result_chart/([0-9]{1,3})/$', views.districtResultChart),
 	url(r'ajax/district_demog_chart/([0-9]{1,3})/$', views.districtDemogChart),
 	url(r'ajax/county_result_chart/([0-9]{1,3})/$', views.countyResultChart),
 	url(r'ajax/county_demog_chart/([0-9]{1,3})/$', views.countyDemogChart),
 	url(r'ajax/maps/precinctload/([0-9]{1,3})/$', views.countydynamicmapajax ),
 	url(r'ajax/districtincounty_result_chart/$', views.districtInCountyResultChart),
 	url(r'ajax/districtincounty_demog_chart/$', views.districtInCountyDemogChart),
 	url(r'maps/([0-9]{1,3})/$', views.dynamicmap),
 	url(r'ajax/maps/precinctloaddistrict/([0-9]{1,3})/$', views.dynamicmapajax ),
 	#district type report
 	url(r'officereport/([0-9]{1,2})/$', views.officeTypeReport),
 	url(r'officereportajax/', views.officeTypeReportAjax),
 	url(r'electionresults/([a-zA-Z]{2})', views.electionResultPage),
 	url(r'officereportajax2/', views.officeTypeReportAjax2),
 	url(r'$', views.frontpage )
]


{
				"pop_hisp": "Hispanic Population by Precinct",
				"pop_anglo": "Anglo Population by Precinct",
				"pop_other": "Population by Precinct, Not Hispanic, Anglo, or Black",
				"pop_total": "Total Population by Precinct",
				"vap_total": "Voting Age Population by Precinct",
				"pop_black": "Black Population by Precinct",
				"pop_blhisp": "Black and Hispanic Population by Precinct",
				"vap_anglo": "Voting Age Population by Precinct, Anglo",
				"vap_hisp": "Voting Age Population by Precinct, Hispanic",
				"vap_blhisp": "Voting Age Population by Precinct, Black and Hispanic",
				"vap_black": "Voting Age Population by Precinct, Black",
				"vap_other": "Voting Age Population by Precinct, Not Hispanic, Anglo, or Black",
				}
function lineChart(dataurl, divid, xdata, ydata1, ydata2) {
		// set the dimensions and margins of the graph
			var margin = {top: 20, right: 20, bottom: 50, left: 50},
			    width = 800 - margin.left - margin.right,
			    height = 500 - margin.top - margin.bottom;

			 var color = ['steelblue', 'red'];
			 var drawarray = [ydata1, ydata2];

		d3.json(dataurl, function(error, data) {
			if (error) throw error;
			console.log(data)

			// set the ranges
			var x = d3.scaleTime().range([0, width]);
			var y = d3.scaleLinear().range([height, 0]);

			var parseTime = d3.timeParse('%Y')	
			data.forEach(function (d) {
				d.year = d[xdata],
				d[xdata] = parseTime(d[xdata]);
			})

			x.domain(d3.extent(data, function(d) { return d[xdata]; }));
			y.domain([0, d3.max(data, function(d) {
				  return Math.max(d[ydata2], d[ydata1] )})]);

			// define the 1st line
			var valueline = d3.line()
				// .defined(function(d) {return d.y != null && d.y != undefined})
			    .x(function(d) {  return x(d[xdata]) })
			    .y(function(d) { return y(d[ydata1]); })
			    .curve(d3.curveMonotoneX);

			// define the 2nd line
			var valueline2 = d3.line()
			    .x(function(d) { return x(d[xdata]); })
			    .y(function(d) { return y(d[ydata2]); })
			     .curve(d3.curveMonotoneX);

			// append the svg obgect to the body of the page
			// appends a 'group' element to 'svg'
			// moves the 'group' element to the top left margin
			var svg = d3.select('#' + divid).append("svg")
			    .attr("width", width + margin.left + margin.right)
			    .attr("height", height + margin.top + margin.bottom)
			  .append("g")
			    .attr("transform",
			          "translate(" + margin.left + "," + margin.top + ")");

			var tooltip = d3.select('#' + divid)
				.append('div').attr('class', 'tooltip');
				
				// .attr('visibility', 'hidden');
			 
			  // Add the valueline path.
			svg.append("path")
			      .datum(data)
			      .attr("class", "line")
			      .style("stroke", color[0])
			      .attr("d", valueline)

			  // Add the valueline2 path.
			svg.append("path")
			      .data([data])
			      .attr("class", "line")
			      .style("stroke", color[1])
			      .attr("d", valueline2);


			//create class that allows us to label all the circles on same x axis point (e.g. the same year)
			//on the same domain.

			// var circleClass = function() {
			// 	var xvalCircle = function (d) { return d[xdata] };
			// 	return 'circle' + xvalCircle().toString();			
			// };


			//create new group into which we will append circles.
			var svgCircles = svg.append('g')
				.attr('class', 'hovercircle');

			//create circles that will add hover effect.
			svgCircles.selectAll('hovercircle1')
				.data(data)
				.enter().append('circle')
				.attr('r', '4')
				.attr('cx', function(d) {  return x(d[xdata]); })
				.attr('cy', function(d) { return y(d[ydata1]); })
				.attr('fill', color[0])
				.attr('class', function (d) { return d.year } )
				.on('mouseover', function (d){
				tooltip.transition()		
                	.duration(200)		
                	.style("opacity", .9)
                	.style("left", (d3.event.pageX) + "px")		
                	.style("top", (d3.event.pageY) + "px"); 
					tooltip.html( '<p>' + d.year + "<br/>" + ydata1.toString() + ': ' + d[ydata1] + "<br/>" + ydata2.toString() + ': ' + d[ydata2] + '</p>' ); 
					svg.append('line')
						.style('stroke', 'purple')
						.attr('x1', x(d[xdata]))
						.attr('y1', 0)
						.attr('x2', x(d[xdata]))
						.attr('y2', height); 
					})
				.on("mouseout", function(d) {		
		            tooltip.transition()		
		               .duration(500)		
		               .style("opacity", 0);
		            svg.selectAll('line').remove();		
				});
// 	
			svgCircles.selectAll('hovercircle2')
				.data(data)
				.enter().append('circle')
				.attr('r', '4')
				.attr('cx', function(d) {  return x(d[xdata]); })
				.attr('cy', function(d) { return y(d[ydata2]); })
				.attr('fill', color[1])
				.attr('class',  function (d) { return d.year } )
				.on('mouseover', function (d){
				tooltip.transition()		
                	.duration(200)		
                	.style("opacity", .9)
                	.style("left", d3.event.pageX)		
                	.style("top", d3.event.pageY); 
					tooltip.html( '<p>' + d.year + "<br/>" + ydata1.toString() + ': ' + d[ydata1] + "<br/>" + ydata2.toString() + ': ' + d[ydata2] + '</p>' ); 
					svg.append('line')
						.style('stroke', 'purple')
						.attr('x1', x(d[xdata]))
						.attr('y1', 0)
						.attr('x2', x(d[xdata]))
						.attr('y2', height); 
					})
				.on("mouseout", function(d) {		
		            tooltip.transition()		
		               .duration(500)		
		               .style("opacity", 0);
		            svg.selectAll('line').remove();		
				});
		

			/// add the x-axis
			svg.append("g")
			      .attr("transform", "translate(0," + height + ")")
			      .call(d3.axisBottom(x).ticks(d3.timeYear.every(2)));
			  
			svg.append("g")
			      .call(d3.axisLeft(y));

			var legendcontainer = svg.append('g').attr('class', 'legend');
			var legendscale = d3.scaleLinear().range([0,150]).domain([1, color.length]);

			var legend = legendcontainer.selectAll('.legend')
			  	.data(color).enter()
			  	.append('g')
			  	.attr('class', 'legendItem')
			  	
			  	
			  	
			legend.append('rect')
			  	.attr('width', '20')
			  	.attr('height', '10')
			  	.attr('fill', function(d) {return d})
			  	.attr('x', function (d, i) {return (legendscale(i+1) + (width - (width *.6)))})
			  	.attr('y', (height + (margin.bottom / 2)));
			  	
			   
			  
			  legend.append('text')
			  	.text(function (d, i) { return drawarray[i] })
			  	.attr('class', 'charttext')
			  	.attr('x', function (d, i) {return legendscale(i+1) + (width - (width *.6)) + 25})
			  	.attr('y', (height + (margin.bottom / 2) + 8))
			  	.attr('text-anchor', 'center');
				  


			});//end d3.json call
		}//linechart